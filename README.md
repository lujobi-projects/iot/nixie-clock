## Nixie Clock

This repo contains the code of my nixie clock.

Due to some construction mistakes, some parts of the code fix the hardware issues. I will try to fix them in the next version of the PCB. Namely the following:

- Two tubes have been swapped, so the code swaps them back
- The tubes are designed to be installed the wrong way around, so the code swaps the digits back
