// needs:
// - http://arduino.esp8266.com/stable/package_esp8266com_index.json,
// - FastLED: https://github.com/FastLED/FastLED (via library manager)
// - https://github.com/arduino-libraries/NTPClient (via library manager)

#include <ESP8266WiFi.h>
#include <FastLED.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

// Shift registers
constexpr uint8_t PIN_SR_OE = 15;    // D8:
constexpr uint8_t PIN_SR_SEL3 = 13;  // D7:
constexpr uint8_t PIN_SR_SEL2 = 12;  // D6:
constexpr uint8_t PIN_SR_SEL1 = 14;  // D5:
constexpr uint8_t PIN_SR_RCLK = 2;   // D4:
constexpr uint8_t PIN_SR_SER = 0;    // D3:

// LED Data
constexpr uint8_t PIN_LED_DATA = 4;  // D2
constexpr uint8_t NUM_LEDS = 6;

// Buttons
constexpr uint8_t PIN_BUTTON_U1 = 9;   // SD2: DEACTIVATED
constexpr uint8_t PIN_BUTTON_U2 = 8;   // SD1: DEACTIVATED
constexpr uint8_t PIN_BUTTON_U3 = 5;   // D1: DEACTIVATED
constexpr uint8_t PIN_BUTTON_U4 = 16;  // D0: DEACTIVATED

// Transisor
constexpr uint8_t PIN_HV_TR = 10;  // SD3: Transistor for the high voltage current

const String gk_ssid = "***";
const String gk_password = "***";

WiFiUDP ntpUDP;

// By default 'pool.ntp.org' is used with 60 seconds update interval and
// no offset
NTPClient timeClient(ntpUDP, 7200);

CRGB g_leds[NUM_LEDS];

using ShiftRegisterState = std::array<uint8_t, 3>;
enum class ShiftRegister : uint8_t {
  S1 = 0,  // n1, n2
  S2,      // n3, n4
  S3       // n5, n6
};
inline uint8_t to_uint8(ShiftRegister sr) { return static_cast<uint8_t>(sr); }
inline ShiftRegister to_shift_register(uint8_t no) { return static_cast<ShiftRegister>(no); }

// Due to a wrongly wired nixies
inline uint8_t correct_number(uint8_t no) {
  no = no == 0 ? 10 : no;
  return (11 - no) % 10;
}
inline uint8_t number_to_bcd_shift_register(uint8_t no, bool swapped = false) {
  const uint8_t fst = correct_number(no / 10);
  const uint8_t snd = correct_number(no % 10);
  return swapped ? (fst | (snd << 4)) : (snd | (fst << 4));
}

class Display {
  uint8_t left;    // 0-99 (technically)
  uint8_t middle;  // 0-99
  uint8_t right;   // 0-99

 public:
  Display(uint8_t h, uint8_t m, uint8_t s) : left(h), middle(m), right(s) {}

  ShiftRegisterState to_shift_register_state() const {
    return {number_to_bcd_shift_register(right), number_to_bcd_shift_register(middle, true),
            number_to_bcd_shift_register(left, true)};
  }
  String to_string() const { return String(left) + ":" + String(middle) + ":" + String(right); }
  bool operator==(const Display &other) const {
    return left == other.left && middle == other.middle && right == other.right;
  }

  Display &operator++() {
    ++right;
    if (right / 100 != 0) {
      ++middle, right %= 100;
    }
    if (middle / 100 != 0) {
      ++left, middle %= 100;
    }
    left %= 100;
    return *this;
  }
};

inline uint8_t get_shift_register_pin(ShiftRegister sr) {
  switch (sr) {
    case ShiftRegister::S1:
      return PIN_SR_SEL1;
    case ShiftRegister::S2:
      return PIN_SR_SEL2;
    case ShiftRegister::S3:
      return PIN_SR_SEL3;
  }
  return PIN_SR_SEL1;
}

void write_number_to_shift_register(ShiftRegister shift_register, uint8_t value) {
  uint8_t pin = get_shift_register_pin(shift_register);
  shiftOut(PIN_SR_SER, pin, MSBFIRST, value);
  digitalWrite(PIN_SR_RCLK, LOW);
  delay(1);
  digitalWrite(PIN_SR_RCLK, HIGH);
}

inline void set_pin_output_and_default(uint8_t pin, uint8_t value) {
  pinMode(pin, OUTPUT);
  digitalWrite(pin, value);
}

void update_shift_state(const ShiftRegisterState &shift_register_state) {
  static ShiftRegisterState s_shift_reg_state({0, 0, 0});

  for (uint8_t i = 0; i < shift_register_state.size(); i++) {
    if (s_shift_reg_state.at(i) != shift_register_state.at(i)) {
      write_number_to_shift_register(to_shift_register(i), shift_register_state.at(i));
      s_shift_reg_state.at(i) = shift_register_state.at(i);
    }
  }
}

void setLedColor(CRGB col) {
  static CRGB g_col = CRGB::Black;
  if (g_col != col) {
    for (uint8_t i = 0; i < NUM_LEDS; i++) {
      g_leds[i] = col;
    }
    g_col = col;
    FastLED.show();
    FastLED.show();
  }
}

void blink_display() {
  static uint8_t s_counter = 0;
  s_counter = (s_counter + 1) % 10;
  const uint8_t no = number_to_bcd_shift_register(11 * s_counter);
  update_shift_state({no, no, no});
}

void setup() {
  Serial.begin(57600);

  for (const auto &pin : {PIN_SR_SEL1, PIN_SR_SEL2, PIN_SR_SEL3, PIN_SR_RCLK, PIN_SR_SER, PIN_SR_OE}) {
    set_pin_output_and_default(pin, LOW);
  }
  pinMode(PIN_LED_DATA, OUTPUT);

  FastLED.addLeds<WS2812B, PIN_LED_DATA, GRB>(g_leds, NUM_LEDS);
  setLedColor(CRGB::DarkBlue);

  // setup ntp
  WiFi.begin(gk_ssid, gk_password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    blink_display();
  }
  timeClient.begin();
}

void loop() {
  static Display disp(12, 34, 56);

  timeClient.update();

  disp = Display(timeClient.getHours(), timeClient.getMinutes(), timeClient.getSeconds());
  setLedColor(CRGB::DarkBlue);
  update_shift_state(disp.to_shift_register_state());

  delay(200);  // needs to be below nyquist frequency - aka smaller than 1/2 Hz
}
